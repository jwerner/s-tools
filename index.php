<!doctype html>
<?php

$cfg = new StdClass;
$cfg->siteTitle = 's-Tools';
if(file_exists(__DIR__.'/config.json')) {
    $json = file_get_contents(__DIR__.'/config.json');
    $cfg = json_decode($json, false);
}
?>
<html>
    <head>
        <link rel="stylesheet" href="css/mini-default.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $cfg->siteTitle ?></title>
    </head>
    <body>
        <header>
            <a href="#" class="logo"><?= $cfg->siteTitle ?></a>
            <a href="#" class="button">Home</a>
            <a href="README.md" class="button">About</a>
        </header>
        <div class="section">
            <h3><?= $cfg->applications->title ?></h3>
            <div class="container">
                <div class="row">
                    <?php foreach($cfg->applications->links as $link) : ?>
                    <div class="card">
                        <div class="section">
                            <h3 class="doc"><?= $link->label ?></h3>
                            <p class="doc"><?= isset($link->description) ? $link->description : '' ?> <a href="<?=$link->link ?>" class="button" target="_blank">Link</a></p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="section">
            <h3><?= $cfg->databases->title ?></h3>
            <div class="container">
                <div class="row">
                    <?php foreach($cfg->databases->links as $link) : ?>
                    <div class="card">
                        <div class="section">
                            <h3 class="doc"><?= $link->label ?></h3>
                            <p class="doc"><?= isset($link->description) ? $link->description : '' ?> <a href="<?=$link->link ?>" class="button" target="_blank">Link</a></p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="section">
            <h3><?= $cfg->tools->title ?></h3>
            <div class="container">
                <div class="row">
                    <?php foreach($cfg->tools->links as $link) : ?>
                    <div class="card">
                        <div class="section">
                            <h3 class="doc"><?= $link->label ?></h3>
                            <p class="doc"><?= isset($link->description) ? $link->description : '' ?> <a href="<?=$link->link ?>" class="button" target="_blank">Link</a></p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </body>
