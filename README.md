s-Tools
=======

This is a set of tools to help managing a webspace containing some PHP applications, managing databases and rolling out code updates.

## Installation

```
$ git clone https://bitbucket.org/jwerner/s-tools
$ cd s-tools
$ git submodule init
$ git submodule update
```

## Configuration

### Adminer

Copy __adminer-4.8.1.php__ into the __adminer__ directory

### Tiny Filemanager

Edit __tinyfilemanager/config.php__

### php-git-bundle

Create __php-git-bundle/config.php__

### Adding more links

Edit __config.json__. Add links to the __databases__ or __tools__ sections.
